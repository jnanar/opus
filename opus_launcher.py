#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

from opus import gui_manager
from opus import manager
import os
import sys
import argparse
import logging


def launch():
    """
    Opus launcher: a simple helping script the interface: CLI or GUI.
    """
    parser = argparse.ArgumentParser(description='Opus is a simple program that aims to help find a job.')
    parser.add_argument("-d", "--debug", help="Debug in the log file",
                        action="store_true", default=False, dest='debug')

    parser.add_argument("-v", "--verbose", help="Be verbose, print some infos",
                        action="store_true", default=False, dest='verbose')


    parser.add_argument("-i", "--interface",
                        help="Interface: cli for Command line interface or gui for Graphical user interface",
                        action="store", default="gui", dest='interface')
    parser.add_argument("-c", "--config", help="Configuration file",
                        action="store", default="opus.example.ini", dest='config')
    parser.add_argument("-lf", "--log-file", help="Log file location",
                        action='store', default="opus.log", dest='log_file')
    args = parser.parse_args()
    #print(args)
    debug = logging.INFO
    if args.debug:
        debug = logging.DEBUG
    debug_cl = logging.WARNING
    if args.verbose:
        debug_cl = logging.DEBUG

    if not os.path.exists(args.config):
        print("Error : Config file not found")
        parser.print_help()
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

    try:
        # create logger with 'opus'
        logger = logging.getLogger('opus')
        logger.setLevel(logging.INFO)
        # create file handler which logs errors
        #try:
        fh = logging.FileHandler(args.log_file)
        #
        fh.setLevel(debug)
        # create console handler with a higher log level
        ch = logging.StreamHandler()
        ch.setLevel(debug_cl)
        # create formatter and add it to the handlers
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)
        # add the handlers to the logger
        logger.addHandler(fh)
        logger.addHandler(ch)

        if args.interface == 'cli':
            m = manager.FindMeJobs(path='./', config_file=args.config)
            ret = m.start()
        if args.interface == 'gui':
            m = gui_manager.GuiManager(path='./', config_file=args.config)
            ret = m.start_gui()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


if __name__ == '__main__':
    launch()
