Opus est un programme d'aide à la recherche d'emploi. Je l'ai codé lorsque je recherchais un boulot. Après avoir reçu retour positifs sur l'existence d'un tel programme, je le rend disponible pour le plus grand nombre. 

Opus facilite l'envoi de lettres de motivation et CV générés à partir de canevas personnalisés. Il permet également d'envoyer des emails adaptés au destinataire. Les données sont extraites à partir d'éléments encodés préalablement dans un tableur. 

En d'autres termes, c'est un système de publipostage ciblé pour la recherche d'emploi.

**Opus est en version alpha. L'utilisateur est responsable de son utilisation est des messages envoyés.**

# Installation

Opus est un programme écrit en python. Il nécessite les dépendances suivantes (installables via pip):

- appdirs
- lxml
- xlrd
- PyQt5

Pour l'instant, le plus simple est de cloner le dépôt git après avoir installé les dépendances dans un environement virtuel. Un script d'installation est prévu prochainement.

```
 git clone https://gitlab.com/jnanar/opus.git
 cd opus
 virtualenv venv
 source venv/bin/activate
 pip install .
```
Configuration du programme (voir section [méthodologie](#methodologie))
```
 python3 opus.py
```

Par ailleurs, si vous voulez utiliser l'interface graphique, vous devez également installer pyQt 5.

Il est également possible de télécharger les sources sous forme d'archive zip depuis le [dépôt](https://gitlab.com/jnanar/opus/tree/master).
 
# Méthodologie

La procédure d'utilisation est composée de 9 étapes.

1. Copie du fichier de configuration opus.example.ini vers opus.ini.
2. Personalisation du fichier de configuration (voir la section [Configuration](#config)).
3. Edition du répertoire (par défaut data/list.xlsx) à l'aide d'un tableur (OpenOffice, libreOffice, MS Office, etc). (voir section [Édition du tableau](#conftable))
4. Personalisation des lettres de motivations. A l'heure actuelle, deux canevas sont prévus: le premier pour les candidatures spontanées et le second en cas de réponse à une offre.
   Il est primordial de soigner ces lettres afin qu'elles soient uniques et personnelles. Opus permet d'adapter légèrement les lettres en fonction du destinataire, de la compagnie mais si la lettre est mauvaise, Opus, ne vous sera d'aucune utilité. (voir section [Édition des lettres](#conf_letters))
5. Lancement du programme opus.py. Deux interfaces sont disponibles: une textuelle et une interface graphique (voir section [Interfaces](#interfaces)).
La première étape consiste à générer les lettres et les emails. Les canevas des documents sont spécifiés dans le fichier de configuration. Les fichiers générés sont placés dans le répertoire data/sent par défaut (option out_dir du fichier de configuration). Il est indispensable de les éditer. En effet, les modification apparaissent en gras et il est nécessaire de vérifier les accords, la tournure des phrases et la cohérence de l'ensemble.
6. L'étape suivante consiste à convertir les lettres au format PDF. 
7. La dernière étape consiste à envoyer les mails. Une copie est envoyée à votre adresse en copie cachée. 
8. Une fois les mails envoyés, il est nécessaire d'éditer le fichier le tableau en indiquant un commentaire dans la première colonne afin de ne pas renvoyer plusieurs fois la même candidature par inadvertance.
9. Il est conseillé de déplacer les fichiers docx, PDF et les mails dans un sous-dossier afin de garder une trace des éléments envoyés (tracabilité et éventuelles justification auprès des services administratifs).

Le mieux est probablement de réaliser plusieurs tests avec votre adresse personnelle.

Attention, la génération des PDF n'est pas encore possible sous windows. La solution nécessite pour l'instant la présence de de l'exacutable `soffice` pour réaliser cette tâche. Un script `.bat` sera préparé prochainement. 

D'autres possibilités existent et seront codées si une demande est formulée dans ce sens. En attendant, il est possible de générer les PDF manuellement dans votre éditeur de document préféré.

# Interfaces
Deux interfaces sont disponibles: textuelle et graphique. Elles sont toutes les deux faciles d'utilisation et l'utilisateur peut les choisir au moyen de l'option `-i` ou `--interface` du script de lancement `opus.py`. Par défaut, l'interface graphique est sélectionnée.

# Interface graphique (GUI)
L'interface graphique est intuitive. Elle permet de réaliser toutes les tâches d'Opus mais d'également de choisir à la volée un fichier de configuration et d'enregistrer les modifications réalisées. L'état du logitiel est affiché dans une barre d'état, ce qui permet de suivre l'avancement des opérations facilement.
![Opus Gui: Cover letter generation](opus_gui_1.png)
![Opus Gui: Settings](opus_gui_2.png)
![Opus Gui: About](opus_gui_3.png)
![Opus Gui: SMTP](opus_gui_4.png)

# Interface textuelle (CLI)
L'interface textuelle permet d'utiliser Opus de manière linéaire et facile. Le programme pose des questions (pour l'instant uniquement en français). A la fin de chaque étape, le script demande si l'utilisateur a fait le nécessaire pour passer à l'étape suivante. Il est possible de relancer plusieurs fois Opus pour sauter certaines étapes et reprendre un travail en cours (par exemple pour convertir en PDF des lettres préalablement éditées).
![Opus Cli](opus-cli.png)




# Configuration

Le fichier `.ini` contient différents champs. 

## Informations personnelles

Les informations personnelles comportent:

- votre nom complet,
- le sujet des mails envoyés pour une candidature spontanée,
- le sujet des mails envoyés en réponse à une offre,
- votre langue (pour l'instant, seul le français est supporté).  


## Les fichiers

Les informations de la section `files` concernent les 

- les canevas des lettres de motivation,
- Le fichier contenant le CV,
- les fichiers générés,
- le répertoire utilisé (data/list.docx par défaut),
- les canevas de mails
- le dossier de destination des fichiers générés (par défaut /data/sent/)

## Envoi de courrier électronique

L'envoi courrier électronique nécessite l'utilisation d'un serveur smtp. Les paramètres de configuration dépendent de votre fournisseur mail (gmail, hotmail, votre fournisseur d'accès internet, etc). Les paramètres sont:

- l'adresse du serveur
- le nom d'utilisateur
- le mot de passe (stocké en clair pour l'instant)
- le numéro de port.

Si un de ces éléments ne nécessite pas d'être configuré, veuillez indiquer `None`. Voir l'exemple ci-dessous pour un serveur ne nécessitant pas d'authentification:
```
server = example.com
user = None
password = None
port = 25
```

Les informations concernant ces paramètres sont disponibles auprès du fournisseur de votre boîte mail.
# Édition du tableau

Chaque ligne du répertoire correspond à l'envoi d'une candidature. Les informations renseignées dans les différentes colonnes sont interprétées et utilisées dans la lettre de motivation et/ou le mail envoyé. Les différents champs sont:

1. **sent** indiquez quelque chose si la candidature a dété été envoyée. Le programme lis cette ligne uniquement si ce champs est vide. Il s'agit d'une sécurité pour éviter d'envoyer plusieurs fois le même mail mais également d'une aide pour faciliter le suivit des offres.
2. **company**: le nom de la société.
3. **sector**: le domaine dans lequel la société opère.
4. **nom**: si vous adressez directement votre candidature à une personne en particulier, il s'agit de son nom.
5. **gender**: le genre de la personne à qui vous adressez votre candidature. Au choix: "Madame"; "Monsieur"; "Madame, monsieur"
6. **interest**: votre intérêt à postuler dans cette société.
7. **offer**: la référence de l'offre à laquelle vous répondez. Laissez ce champs vide si vous postulez spontanément.
8. **subject**: un complément d'information dans le mail que vous envoyez (référence d'une offre par exemple).
9. **email**: l'adresse mail à laquelle le mail doit être envoyé.
10. **language**: la langue de la candidature. Pour l'instant, seul le français est supporté.
11. **web, telephone, number, information**: renseignements supplémentaires qui permettent de concentrer d'autres informations sur la société.

 
# Edition des lettres

Plusieurs éléments sont éditables.

- **TIME**: la date du jour sera spécifiée
- **GENDER**: Monsieur; Madame ou Madame, Monsieur
- **INTEREST**: votre intérêt pour la société
- **COMPANY**: le nom de la société
- **SECTOR**: le domaine d'activité de la société
- **OFFER**: la référence de l'offre à laquelle vous répondez.

Les éléments des mails sont identiques.

# Futur

En l'état, le programme est utilisable et il répondait à mes besoins lorsque je m'en servais. A l'avenir, plusieurs pistes d'améliorations sont envisageables:

- Création d'un exécutable utilisable sous Windows si je reçois des demandes dans ce sens.
