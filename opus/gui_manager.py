#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
# # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
from . import manager
from . import main_window

logger = logging.getLogger('opus.gui.manager')

class GuiManager:
    def __init__(self, path='./', config_file='opus.ini'):
        self.path = path
        self.config_file = config_file
        self.tabular = None
        self.personal = {}
        self.files = {}
        self.smtp = {}

    def letter_generation_gui(self, manager_instance=None, gen= False):
        """
        Generate the cover letters from the templates. non interactive GUI version
        :return:  none
        """
        # Wait for the user to validate the generated files
        manager_instance.tabular(start_item=0, stop_item=1000, gen=gen)

    def start_gui(self):
        """
        Start the Gui application
        :return: None
        """
        path = './'
        config_file = self.config_file
        manager_instance = manager.FindMeJobs(path, config_file)
        ret = manager_instance.get_parameters()
        if ret:
            logger.critical("verify your config file")
            return 1

        gen = False
        self.letter_generation_gui(manager_instance=manager_instance, gen=gen)
        if not manager_instance.tab_lines:
            msg = "Error during table handling"
            logger.error(msg)
            return 1
        main_window.opus_launcher(manager_instance=manager_instance, path=path, personal=manager_instance.personal,
                                  files=manager_instance.files, smtp=manager_instance.smtp,
                                  config_file=config_file,
                                  data_dir='data/')
