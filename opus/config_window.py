#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QLineEdit
from PyQt5.QtGui import QIcon
import sys
from .config_gui import Ui_Config
from . import config_handler
import logging
from os.path import split, normpath, basename

logger = logging.getLogger('opus.gui.config')


class ConfigDialog(QDialog):
    def __init__(self, parent=None):
        """
        This class open the configuration window
        :param parent:
        :return:
        """
        super(ConfigDialog, self).__init__(parent)
        self.ui = Ui_Config()
        self.ui.setupUi(self)
        # set the actions when the buttons are pressed
        # ui.actionLoad_configuration_file.triggered()
        self.windows_lists = []
        self.personal = {}
        self.files = {}
        self.smtp = {}
        self.ui.load_config.clicked.connect(self.button_select_config_file)
        # Config files dialogs
        # Cover letters
        self.ui.pushButton_cover_spon.clicked.connect(
            lambda: self.button_conf_files(filter="Docx files (*.docx);;",
                                           description="Cover Letter Spontaneous",
                                           destination='cover_letter_spontaneous',
                                            label=self.ui.p_cov_spon))
        self.ui.pushButton_cover_off.clicked.connect(lambda: self.button_conf_files(
            filter="Docx files (*.docx);;",
            description="Cover Letter Offer",
            destination='cover_letter_offer',
            label=self.ui.p_cov_off))
        # CV
        self.ui.pushButton_CV.clicked.connect(lambda: self.button_conf_files(
            filter="PDF file (*.pdf);;",
            description="curriculum vitae",
            destination='cv_filename',
                label=self.ui.p_cv))
        # Directory (list of companies)
        self.ui.pushButton_directory.clicked.connect(lambda: self.button_conf_files(
            filter="Excel file (*.xlsx);;",
            description="Directory",
            destination='directory',
            label=self.ui.p_dir))
        # Mails
        self.ui.pushButton_mail_fr.clicked.connect(lambda: self.button_conf_files(
            filter="Text file (*.txt);;",
            description="Email in french  (*.txt)",
            destination='mail_txt_fr',
            label=self.ui.p_mail_fr))
        self.ui.pushButton_mail_en.clicked.connect(lambda: self.button_conf_files(
            filter="Text file (*.txt);;",
            description="Email in english  (*.txt)",
            destination='mail_txt_en',
            label=self.ui.p_mail_en))
        # Out directory
        self.ui.pushButton_out_dir.clicked.connect(self.button_conf_out_dir)

        self.accepted.connect(self.update_fields)
        self.setWindowIcon(QIcon('icon/opus.ico'))

    def windows_list_append(self, win):
        self.windows_list.append(win)

    def set_windows_list(self, wins):
        self.windows_lists = wins

    def get_windows_list(self):
        return self.windows_lists

    def set_parameters(self, config_filename='', personal={}, files={}, smtp={}):
        """
        Set the parameters after reading the initial config file
        :param config_filename: 
        :param personal: peronal dictionary
        :param files: files dictionary
        :param smtp: smtp dictionary
        :return: None
        """
        self.personal = personal
        self.files = files
        self.smtp = smtp
        self.config_filename = config_filename

    def return_parameters(self):
        return {'personal': self.personnal, 'files': self.files, 'smtp': self.smtp}

    def populate_config_parameters(self):
        """
        Set default text for settings based on the loaded file
        :return:
        """
        self.ui.p_name.setText(self.personal['name'])
        self.ui.p_email.setText(self.personal['email'])
        self.ui.p_lang.setText(self.personal['language'])
        self.ui.p_subject_spontaneous.setText(self.personal['subject_spontaneous'])
        self.ui.p_subject_offer.setText(self.personal['subject_offer'])
        # self.ui.f_cover_letter_spontaneous.setPlainText(self.files['cover_letter_spontaneous'])
        # self.ui.f_cover_letter_offer.setPlainText(self.files['cover_letter_offer'])
        # self.ui.f_cv_filename.setPlainText(self.files['cv_filename'])
        # self.ui.f_directory.setPlainText(self.files['directory'])
        # self.ui.f_mail_fr.setPlainText(self.files['mail_txt_fr'])
        # self.ui.f_mail_en.setPlainText(self.files['mail_txt_en'])
        # self.ui.f_output_dir.setPlainText(self.files['out_dir'])
        self.ui.p_cov_spon.setText(get_filename(self.files['cover_letter_spontaneous']))
        self.ui.p_cov_off.setText(get_filename(self.files['cover_letter_offer']))
        self.ui.p_cv.setText(get_filename(self.files['cv_filename']))
        self.ui.p_mail_fr.setText(get_filename(self.files['mail_txt_fr']))
        self.ui.p_mail_en.setText(get_filename(self.files['mail_txt_fr']))
        self.ui.p_dir.setText(get_filename(self.files['directory']))
        self.ui.p_out_dir.setText(self.files['out_dir'])

        self.ui.s_server.setText(self.smtp['server'])
        self.ui.s_user.setText(self.smtp['user'])
        self.ui.s_password.setText(self.smtp['password'])
        self.ui.s_port.setText(self.smtp['port'])

    def button_select_config_file(self):
        """
        Select an ini file to load the configuration
        :return: None
        """
        logger.debug("select file")
        filters = "Ini Text files (*.ini);;"
        fd = QFileDialog()
        file_object = QFileDialog.getOpenFileName(fd, 'Open config file', '~/', filters)
        self.config_filename = file_object[0]
        logger.debug(file_object[0])
        s = config_handler.ConfHandler(file_object[0])
        config = s.read_config()
        self.personal = config['personal']
        self.files = config['files']
        self.smtp = config['smtp']
        self.populate_config_parameters()

    def update_fields(self):
        self.personal['name'] = self.ui.p_name.text()
        self.personal['email'] = self.ui.p_email.text()
        self.personal['language'] = self.ui.p_lang.text()
        self.personal['subject_spontaneous'] = self.ui.p_subject_spontaneous.text()
        self.personal['subject_offer'] = self.ui.p_subject_offer.text()
        # self.files['cover_letter_spontaneous'] = self.ui.f_cover_letter_spontaneous.toPlainText()
        # self.files['cover_letter_offer'] = self.ui.f_cover_letter_offer.toPlainText()
        # self.files['cv_filename'] = self.ui.f_cv_filename.toPlainText()
        #self.files['directory'] = self.file_dir
        # self.files['mail_txt_fr'] = self.ui.f_mail_fr.toPlainText()
        # self.files['mail_txt_en'] = self.ui.f_mail_en.toPlainText()
        # self.files['out_dir'] = self.ui.f_output_dir.toPlainText()
        self.smtp['server'] = self.ui.s_server.text()
        self.smtp['user'] = self.ui.s_user.text()
        self.smtp['password'] = self.ui.s_password.text()
        self.smtp['port'] = self.ui.s_port.text()
        # save the modified configuration file.
        s = config_handler.ConfHandler(self.config_filename)
        s.write_config(personal=self.personal, files=self.files, smtp=self.smtp, path_ini=self.config_filename)

    def button_conf_files(self, filter=None, description=None, destination=None, label=None):
        logger.debug("select file")
        filters = filter
        fd = QFileDialog()
        file_object = QFileDialog.getOpenFileName(fd, description, '~/', filters)
        self.files[destination]= file_object[0]
        label.setText(get_filename(file_object[0]))
        logger.debug(file_object[0])


    def button_conf_out_dir(self):
        logger.debug("select output dir")
        fd = QDialog()
        directory = QFileDialog.getExistingDirectory(self, "Select Output Directory")
        self.files['out_dir'] = str(directory) + '/'
        dir_name = basename(normpath(self.files['out_dir']))
        self.ui.p_out_dir.setText(dir_name)
        logger.debug(self.files['out_dir'])

def get_filename(full_path):
    return split(full_path)[1]

def config_launcher():
    app = QApplication(sys.argv)
    config_gui = ConfigDialog()
    config_gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    config_launcher()
