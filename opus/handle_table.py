#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
# # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

import xlrd
import logging

logger = logging.getLogger('opus.handle_table')


class Excel:
    def __init__(self):
        self.sheet = None

    def open_file(self, path):
        """
         Open and read an Excel file
        :param path: path of the list file
        """
        try:
            book = xlrd.open_workbook(path)
            self.sheet = book.sheet_by_name('base')
        except FileNotFoundError:
            self.sheet = None
            msg = "Directory file {} not found.".format(path)
            logger.critical(msg)
            return 1
        return 0

    def row_i(self, i):
        """

        :param i: line number
        :return: row i
        """
        line = {}
        try:
            row = self.sheet.row_slice(rowx=i,
                                       start_colx=0,
                                       end_colx=11)

            line['spontaneous'] = True
            line['company'] = row[1].value
            line['already_sent'] = row[0].value
            if line['already_sent'] == u'' and line['company'] != u'':
                line['sector'] = row[2].value
                line['name'] = row[3].value
                line['gender'] = row[4].value
                line['interest'] = row[5].value
                line['offer'] = row[6].value
                line['subject'] = row[7].value
                line['email'] = row[8].value
                line['lang'] = row[9].value
                try:
                    if len(line['offer']) > 0:
                        line['spontaneous'] = False
                except TypeError:
                    logger.error("Verify directory: invalid data {}".format(line))
                    pass
            else:
                return {}
        except IndexError:
            msg = "Row {} out of range !".format(i)
            logger.info(msg)
            return "STOP"
        return line


# ----------------------------------------------------------------------
if __name__ == "__main__":
    e = Excel()
    e.open_file("../data/list.xlsx")
    for i in range(1, 10):
        list = e.row_i(i)
        print(list)
