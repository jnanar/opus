#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
# # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

import configparser
import logging

logger = logging.getLogger('opus.config')


class ConfHandler:
    def __init__(self, path_ini):
        self.path_ini = path_ini

    def config_loader(self):
        config = configparser.ConfigParser()
        config.read(self.path_ini)
        return config

    @staticmethod
    def configsectionmap(config, section):
        dict1 = {}
        options = config.options(section)
        for option in options:
            try:
                dict1[option] = config.get(section, option)
                if dict1[option] == -1:
                    logger.info("skip: %s" % option)
            except:
                logger.critical("exception on %s!" % option)
                dict1[option] = None
        return dict1

    def read_config(self):
        try:
            section = 'personal'
            personal = {}
            c = self.config_loader()
            personal = self.get_parameters(dict=personal, section=section,
                                           list=['name', 'email', 'subject_spontaneous',
                                                 'subject_offer', 'language'], config_instance=c)
#            print(personal)
            section = "smtp"
            smtp = {}
            smtp = self.get_parameters(dict=smtp, section=section, list=['server', 'user', 'password', 'port'],
                                       config_instance=c)
#            print(smtp)
            section = 'files'
            files = {}
            files = self.get_parameters(dict=files, section=section, list=['cover_letter_spontaneous','cover_letter_offer',
                                                                           'cv_filename', 'directory','mail_txt_fr',
                                                                           'mail_txt_en', 'out_dir'], config_instance=c)

            # section = 'opus'
            # opus_conf = {}
            # opus_conf = self.get_parameters(dict=opus_conf, section=section, list=['log_file',
            #                                                                        'log_level_file',
            #                                                                        'log_level_console'],
            #                                 config_instance=c)

        except configparser.NoSectionError:
            msg = "Error while loading config file. Verify your options. Missing section"
            logger.critical(msg)
            return None
#        print(files)
        if smtp is not None and personal is not None and files is not None:
#            r = {'smtp': smtp, 'personal': personal, 'files': files, 'opus': opus_conf}
            r = {'smtp': smtp, 'personal': personal, 'files': files}
        else:
            r = None
        return r

    def get_parameters(self, dict={}, section='', list=[], config_instance=None):
        for i in list:
            try:
                dict[i] = config_instance.get(section, i)
            except configparser.NoOptionError:
                msg = "Error in config file: {} : {}".format(section, i)
                logger.critical(msg)
                return None
        return dict


    def write_config(self, path_ini='', personal={}, files={}, smtp={}):
        """
            Write a modified configuration into a file.
        :rtype: object
        """
        config = configparser.ConfigParser()
        config.add_section('personal')
        config.add_section('files')
        config.add_section('smtp')
        self.path_ini = path_ini
        for s in personal:
            config.set('personal', s, personal[s])
        for s in files:
            config.set('files', s, files[s])
        for s in smtp:
            config.set('smtp', s, smtp[s])

        with open(self.path_ini, 'w') as configfile:
            config.write(configfile)





if __name__ == '__main__':
    # Setup the command line arguments.
    path = '../opus.example.ini'
    s = ConfHandler(path)
    r = s.read_config()
