#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QStatusBar
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt, QThread
import sys
from .opus_gui import Ui_Opus
from .config_window import ConfigDialog
from . import workers
from . import manager
import logging
from time import sleep

logger = logging.getLogger('opus.gui.main_window')


class OpusWindow(QMainWindow):
    def __init__(self, parent=None):
        """
        This is the main window of Opus
        :param parent:
        :return:
        """
        super(OpusWindow, self).__init__(parent)
        self.app_name = "Opus"
        self.version = "1.0b"
        self.state = None
        self.ui = Ui_Opus()
        self.ui.setupUi(self)
        # when File --> quit is pressed, the application is closed
        self.ui.actionExit.triggered.connect(self.close)
        self.ui.actionConfiguration.triggered.connect(self.open_config)
        self.ui.actionHelp_Opus.triggered.connect(self.help_opus)
        self.ui.actionAbout.triggered.connect(self.about_opus)
        self.ui.mail_button.clicked.connect(self.mail_clicked)
        self.ui.generate_button.clicked.connect(self.generate_clicked)
        self.ui.convert_button.clicked.connect(self.convert_clicked)
        self.setWindowIcon(QIcon('icon/opus.ico'))
        self.files = {}
        self.personal = {}
        self.smtp = {}
        self.manager_instance = None
        self.status_bar = QStatusBar()
        self.ui.toolBar.addWidget(self.status_bar)
        self.status_bar.showMessage("Ready")
        self.config_file = ''
        self.msg = None

    def set_parameters(self, path='./', personal={}, files={}, smtp={}, config_file='opus.ini', data_dir='',
                       manager_instance=None):
        """
        Set the parameters after reading the initial config file
        :param path:
        :param personal:
        :param config_file:
        :param data_dir:
        :param manager_instance:
        :param files: files dictionnary
        :param smtp: smtp dictionnary
        :return: None
        """
        self.data_dir = data_dir
        self.path = path
        self.personal = personal
        self.files = files
        self.smtp = smtp
        self.config_file = config_file
        if not self.manager_instance:
            self.manager_instance = manager.FindMeJobs(path, config_file)
        self.manager_instance.set_parameters_gui(files=self.files, personal=self.personal, smtp=self.smtp, path=path,
                                                 data_dir=data_dir)
        self.manager_instance.letter_generation_gui(False)

    def generate_clicked(self):
        self.status_bar.showMessage('Busy: Letter generation')
        logger.debug("Generate clicked")
        self.manager_instance.set_parameters_gui(files=self.files, personal=self.personal, smtp=self.smtp,
                                                 path=self.path,
                                                 data_dir=self.data_dir)
        #self.manager_instance.letter_generation_gui(gen)
        self.obj = workers.GenerateWorker(manager_instance=self.manager_instance)  # no parent!
        self.thread = QThread()
        self.obj.moveToThread(self.thread)
        self.obj.finished.connect(self.on_task_done)
        self.thread.started.connect(self.obj.generator)
        self.thread.start()


        self.status_bar.showMessage('Ready')

    def convert_clicked(self):
        """
        When the convert button is clicked
        :return:
        """
        self.status_bar.showMessage('Busy: PDF conversion')
        self.manager_instance.set_parameters_gui(files=self.files, personal=self.personal, smtp=self.smtp,
                                                 path=self.path,
                                                 data_dir=self.data_dir)
        logger.debug("Convert clicked")
        # self.manager_instance.pdf_generation_gui()
        self.obj = workers.ConvertWorker(manager_instance=self.manager_instance)  # no parent!
        self.thread = QThread()  # no parent!
        # 2 - Connect Worker`s Signals to Form method slots to post data.
        #self.obj.pdf_converter.connect(self.on_worker_ready)
        # 3 - Move the Worker object to the Thread object
        self.obj.moveToThread(self.thread)
        # 4 - Connect Worker Signals to the Thread slots
        self.obj.finished.connect(self.on_task_done)
        # 5 - Connect Thread started signal to Worker operational slot method
        self.thread.started.connect(self.obj.pdf_converter)
        # * - Thread finished signal will close the app if you want!
        # self.thread.finished.connect(app.exit)
        self.obj
        # 6 - Start the thread
        self.thread.start()

    def on_task_done(self):
        self.thread.quit()
        if not self.msg:
            self.status_bar.showMessage('Ready')
        else:
            self.status_bar.showMessage(self.msg)
            self.msg = None

    def mail_clicked(self):
        """
        When user want to send his mails
        :return: None
        """
        self.status_bar.showMessage('Busy: sending mails')
        logger.debug("Mail clicked")
        self.manager_instance.set_parameters_gui(files=self.files, personal=self.personal, smtp=self.smtp,
                                                 path=self.path,
                                                 data_dir=self.data_dir)
        self.obj = workers.MailSenderWorker(manager_instance=self.manager_instance)  # no parent!
        self.thread = QThread()
        self.obj.moveToThread(self.thread)
        self.obj.finished.connect(self.on_task_done)
        self.thread.started.connect(self.obj.mail_sender)
        self.thread.start()




    def open_config(self):
        """
        This function is executed when user click on the configuration menu.
        :return: None
        """
        dialog = ConfigDialog()
        dialog.setAttribute(Qt.WA_DeleteOnClose)
        dialog.set_parameters(config_filename=self.config_file, personal=self.personal, files=self.files, smtp=self.smtp)
        dialog.populate_config_parameters()
        ret = dialog.exec_()
        self.config_file = dialog.config_filename
        if ret:
            # The user has validated his change of configuration
            logger.debug(dialog.personal)
            logger.debug(dialog.files)
            logger.debug(dialog.smtp)
            self.personal = dialog.personal
            self.files = dialog.files
            self.smtp = dialog.smtp

    def help_opus(self, event):
        """
        Help window
        :param event:
        :return: None
        """
        qm = QMessageBox()
        qm.setWindowIcon(QIcon('icon/opus.ico'))
        qm.setTextFormat(Qt.RichText)
        url = "<a href=\"https://gitlab.com/jnanar/opus\">" \
              "Visit the Gitlab page of the project to obtain some help.</a>"
        QMessageBox.question(qm, 'Help', url, QMessageBox.Ok)

    def about_opus(self, event):
        """
        About window
        :param event: click ok to close
        :return:
        """
        qm = QMessageBox()
        qm.setWindowIcon(QIcon('icon/opus.ico'))
        qm.setWindowTitle("About Opus")
        qm.setTextFormat(Qt.RichText)
        url = """https://blog.agayon.be"""
        email = "info@agayon.be"
        license_link = "https://www.gnu.org/licenses/gpl-3.0.en.html"
        license_name = "GPLv3"
        qm.setText("<br><br>Opus aims to help you to find a job.</br>" +
                   "<br>It can send multiple cover letters and CVs from templates.</br>" +
                   "<br></br> <br></br>" +
                   self.app_name +
                   " v" +
                   self.version +
                   "</br></br> <br></br>" +
                   "&copy;2017 Arnaud Joset<br><br>" +
                   "<a href='{0}'>{0}</a><br><br>".format(url) +
                   "<a href='mailto:{0}'>{0}</a><br><br>".format(email) +
                   "License: <a href='{0}'>{1}</a>".format(license_link, license_name))
        qm.setStandardButtons(QMessageBox.Ok)
        qm.exec_()


def opus_launcher(path='./', config_file='opus.ini', personal={}, files={}, smtp={}, data_dir='data/',
                  manager_instance=None):
    """
    Launch the Opus gui
    :param path:
    :param personal:
    :param data_dir:
    :param manager_instance:
    :param config_file:
    :param smtp:
    :param files:
    :return: None
    """
    app = QApplication(sys.argv)
    opus_gui = OpusWindow()
    opus_gui.set_parameters(path=path, personal=personal, files=files, smtp=smtp, config_file=config_file,
                            data_dir=data_dir, manager_instance=manager_instance)
    opus_gui.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    opus_launcher()
