#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import logging
import zipfile
import subprocess
import time

logger = logging.getLogger('opus.cover_letter')


class Chdir:
    def __init__(self, newpath):
        self.savedPath = os.getcwd()
        os.chdir(newpath)

    def back(self):
        os.chdir(self.savedPath)


class Word:
    def __init__(self):
        logger.debug("Start word processing")
        self.data_dir = ""
        self.out_dir = ""

    def set_out_dir(self, out_dir):
        """

        :param out_dir: path where the files are saved
        :return: None
        """
        self.out_dir = out_dir

    def set_data_dir(self, data_dir):
        """

        :param data_dir:
        :return: None
        """
        self.data_dir = data_dir

    def cover_letter(self, gender="Madame, Monsieur", spontaneous=False, sector="SECTOR",
                     interest="INTEREST", offer="REF", company="COMPANY", filename="FILENAME",
                     language="FR", data_dir="data/sent", cover_vacancy_filename="cover.docx",
                     cover_spontaneous_filename='cover.docx'):
        logger.debug("Start Cover letter")
        self.data_dir = data_dir
        mounts = []
        if language == "FR":
            mounts = ["janvier", u"février", "mars", "avril", "mai", "juin",
                      "juillet", u"août", "septembtre", "octobre"]
        if language == "EN":
            # Date is in the template
            # FIXME we need to display date in english
            mounts = ['January', 'February', 'March', 'April', 'Apirl', 'May', 'June', 'July', 'September',
                      'October', 'November', 'December', ]
        date = time.strftime('%d') + " "
        try:
            date += mounts[time.localtime()[1] - 1]
        except IndexError:
            logger.error("Specify language for company {}".format(company))
            return None
        date += " " + time.strftime('%Y')
        document = None
        if spontaneous:
            template = cover_spontaneous_filename
        else:
            template = cover_vacancy_filename
        msg = 'Debug line: '
        for s in [gender, data_dir, date, spontaneous, sector, interest, offer, company, filename, language]:
            msg += str(s) + ' '
        logger.debug(msg)
        filename_full_path = self.out_dir + filename
        filename_full_path = filename_full_path.replace(" ", "_")
        replacements = {"TIME": date, "COMPANY": company, "GENDER": gender, "SECTOR": sector,
                        "INTEREST": interest, "OFFER": offer}
        self.docx_replace(template, filename_full_path, replacements)
        return filename

    def docx_replace(self, old_file, new_file, replacements):
        try:
            zin = zipfile.ZipFile(old_file, 'r')
        except FileNotFoundError:
            logger.critical("File {} not found".format(old_file))
            return 1
        zout = zipfile.ZipFile(new_file, 'w')
        for item in zin.infolist():
            buffer = zin.read(item.filename)
            if item.filename == 'word/document.xml':
                res = buffer.decode("utf-8")
                for r in replacements:
                    replacements[r] = self.escape_char(replacements[r])
                    res = res.replace(r, replacements[r])
                buffer = res.encode("utf-8")
            zout.writestr(item, buffer)
        zout.close()
        zin.close()

    def escape_char(self, escape):
        escape = escape.replace("&", "&amp;")
        escape = escape.replace("<", "&lt;")
        escape = escape.replace(">", "&gt;")
        escape = escape.replace("\"", "&quot;")
        escape = escape.replace("'", "&apos;")
        return escape

    def conversion_to_pdf(self):
        """
        This function rely on the presence of Libreoffice if Linux is detected.
        https://www.libreoffice.org/
        Other mechanisms are possible depending on the plateform.
        :return:
        """
        msg = 'PDF conversion'
        logger.info("Conversion to PDF")
        data_dir = self.out_dir
        logger.debug(os.getcwd())
        import opus
        platform = os.name
        logger.debug("System: {}".format(platform))
        if platform == 'posix':
            logger.debug(os.getcwd())
            script_path = os.path.dirname(opus.__file__) + '/bin/'
            # c = Chdir(path + '/bin')
            data_dir = os.path.abspath(data_dir)
            cmd = [script_path + 'convert_to_pdf.sh', data_dir]
            # cmd = ['convert_to_pdf.sh', data_dir]
            try:
                # output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
                output = subprocess.call(cmd, stdout=subprocess.PIPE)
            except subprocess.CalledProcessError:
                logger.error("Exception in DOCX to PDF conversion.")
                msg = "Errors during PDF conversion. Check your files."

        if platform == 'nt':
            logger.debug(os.getcwd())
            script_path = os.path.dirname(opus.__file__) + '/bin/'
            # c = Chdir(path + '/bin')
            data_dir = os.path.abspath(data_dir)
            soffice_exe = r"C:\Program Files (x86)\LibreOffice 5\program\soffice.exe"
            if os.path.isfile(soffice_exe):
                cmd = [script_path + 'convert_to_pdf.bat', data_dir]
                # cmd = ['convert_to_pdf.sh', data_dir]
                try:
                    # output = subprocess.Popen(cmd, stdout=subprocess.PIPE)
                    output = subprocess.call(cmd, stdout=subprocess.PIPE)
                except subprocess.CalledProcessError:
                    logger.error("Exception in DOCX to PDF conversion.")
                    msg = "Errors during PDF conversion. Check your files."
            else:
                logger.error("Libre office is not installed")
                msg = "LibreOffice is not installed. Please install it to use this function"

            return msg
