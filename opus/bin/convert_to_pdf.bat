@ECHO OFF
set export_dir=%1
ECHO Export dir:
ECHO %export_dir%
set OFFICE=C:\Program Files (x86)\LibreOffice 5\program

set OLDDIR=%CD%
cd %export_dir%

ECHO Files to convert:
FOR %%I in (*.docx) DO (
REM FOR %%I in (C:\Users\Arnaud.Tita\Downloads\data\sent\*.docx) DO (
	echo %%I
	"%OFFICE%\soffice.exe" --headless --convert-to pdf %%I

)
chdir /d %OLDDIR% &rem restore current directory