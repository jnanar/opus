#!/bin/sh

#echo "Working directory : " $1

cd "$1"

#echo "----------------------------------------------------------------"

for file in  "$1/"*.docx
do
    #echo $file
    /usr/bin/soffice --headless --convert-to pdf "${file}" 

done


cd -

exit 0
