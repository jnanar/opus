#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot

class GenerateWorker(QThread):
    finished = pyqtSignal()

    def __init__(self, parent=None, manager_instance=None):
        QThread.__init__(self, parent)
        self.manager_instance = manager_instance

    #@pyqtSlot()
    def generator(self):
        gen = True
        self.manager_instance.letter_generation_gui(gen)
        self.finished.emit()

class ConvertWorker(QThread):
    finished = pyqtSignal()

    def __init__(self, parent=None, manager_instance=None):
        QThread.__init__(self, parent)
        self.manager_instance = manager_instance

    #@pyqtSlot()
    def pdf_converter(self):
        msg = self.manager_instance.pdf_generation_gui()
        self.finished.emit()

class MailSenderWorker(QThread):
    finished = pyqtSignal()

    def __init__(self, parent=None, manager_instance=None):
        QThread.__init__(self, parent)
        self.manager_instance = manager_instance

    #@pyqtSlot()
    def mail_sender(self):
        msg = self.manager_instance.send_mails_gui()
        self.finished.emit()


