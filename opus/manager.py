#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
# # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
from . import mailgen
from . import handle_table
from . import cover_letter
from . import config_handler

logger = logging.getLogger('opus.manager')


class FindMeJobs:
    def __init__(self, path='./', config_file='opus.ini'):
        self.path = path
        self.config_file = config_file
        self.data_dir = "data/"
        self.word = cover_letter.Word()
        self.tab_lines = None
        self.smtp = None
        self.personal = None
        self.files = None
        self.opus_conf = None
        self.m = None

    def get_parameters(self):
        """
        Read the config file and polulate the corresponding dictionaries (personal, files and smtp)
        :return: None
        """
        s = config_handler.ConfHandler(self.path + '/' + self.config_file)
        config = s.read_config()
        if not config:
            return 1
        self.smtp = config['smtp']
        self.personal = config['personal']
        self.files = config['files']
#        self.opus_conf = config['opus']
        return 0

    def tabular(self, start_item=1, stop_item=150, gen=False):
        """
        Obtain the parameters from the table.
        :param start_item: first application_parameters of the tabular to use
        :param stop_item: last application_parameters of the tabular to use
        :param gen: generate the motivation letters
        :return: self.tab_lines : the lines of the excel file to handle
        """
        e = handle_table.Excel()
        ret = e.open_file(self.files['directory'])
        if ret:
            return 1
        self.tab_lines = []
        for i in range(start_item, stop_item):
            application_parameters = e.row_i(i)
            if application_parameters == 'STOP':
                # application_parameters is 'STOP' when the current row is out of range
                logger.info("Stop")
                break
            if len(application_parameters) > 0:
                filename = self.letter(i, application_parameters, gen)
                filename.replace("docx", "pdf")
                application_parameters['filename'] = filename
                application_parameters = self.mail_write(application_parameters, gen)
                self.tab_lines.append(application_parameters)

    def mail_write(self, application_parameters, gen):
        """
        Generate the mails
        :param gen:
        :param application_parameters:
        :return: application parameters
        """

        self.m = mailgen.Mail(self.data_dir, self.personal['name'], self.personal['email'], self.smtp)
        mail_filename = "ERROR.TXT"
        if gen:
            # generation of the mails
            my_subject = application_parameters['offer']
            if my_subject == '':
                subject = self.personal['subject_spontaneous']
            else:
                subject = self.personal['subject_offer'] + " "
            my_subject = subject + my_subject
            application_parameters['my_subject'] = my_subject
            mail_filename = self.m.generate_mail(application_parameters=application_parameters,
                                                 mail_txt_fr=self.files['mail_txt_fr'],
                                                 mail_txt_en=self.files['mail_txt_en'], output_dir=self.files['out_dir'],
                                                 my_name=self.personal['name'])
        application_parameters['mail_filename'] = self.files['out_dir'] + mail_filename + '.txt'
        return application_parameters

    def letter(self, item, line, gen):
        """
        Actual cover letter generation from the data
        :param item: row i
        :param line: all the parameters of a company
        :param gen: bool: generate or not the cover letter
        :return: the filename of the cover letter
        """

        line['filename'] = self.personal['name'] + "-" + line['company']
        out_dir = self.files['out_dir']
        self.word.set_out_dir(out_dir)
        # on top of the generated files.
        data_dir = self.files['out_dir']
        data_dir += '../'
        if gen:
            ret = self.word.cover_letter(gender=line['gender'], spontaneous=line['spontaneous'], sector=line['sector'],
                                         interest=line['interest'],
                                         offer=line['offer'], company=line['company'],
                                         filename=line['filename'] + '.docx',
                                         language=line['lang'], data_dir=data_dir,
                                         cover_spontaneous_filename=self.files['cover_letter_spontaneous'],
                                         cover_vacancy_filename=self.files['cover_letter_offer'],
                                         )
            if not ret:
                logger.error("Error company {}".format(line['company']))
        return line['filename'] + '.pdf'

    def letter_generation(self):
        """
        Generate the cover letters from the templates
        :return:  none
        """
        response_yes = ''
        response_no = ''
        msg = ''
        # Wait for the user to validate the generated files
        if self.personal['language'] == 'FR':
            msg = "Voulez-vous générer les lettres et les mails ? (O/n)"
            response_yes = "Génération des fichiers lettres et les mails. \nVeuillez les vérifier."
            response_no = "Vous avez la possibilité de générer les fichiers plus tard. (TODO)"
        if self.personal['language'] == "EN":
            msg = "Do you want to generate the cover letters and the emails ? (Y/n)"
            response_yes = "Generation of the cover letters and the emails"
            response_no = "You can always generate the files later. (TODO)"
        ret = input(msg)
        ret = ret.lower()
        gen = False
        if ret == 'o' or ret == 'y' or ret == '':
            gen = True
            print(response_yes)
        else:
            print(response_no)
        self.tabular(start_item=0, stop_item=1000, gen=gen)

    def pdf_generation(self):
        """
        Generate the PDF from the .docx files
        :return: None
        """
        response_yes = ''
        response_no = ''
        msg = ''
        # Wait for the user to validate the generated files
        if self.personal['language'] == 'FR':
            msg = "Avez-vous adapté les fichiers .docx ? (O/n)"
            response_yes = "Génération des fichiers PDF. \nVeuillez les vérifier."
            response_no = "Vous avez la possibilité de générer les fichiers plus tard. (TODO)"
        if self.personal['language'] == "EN":
            msg = "Did you modified the .docx files ? (Y/n)"
            response_yes = "Generation of the PDF files"
            response_no = "You can always generate the PDFs later. (TODO)"
        ret = input(msg)
        ret = ret.lower()
        if ret == 'o' or ret == 'y' or ret == '':
            self.word.conversion_to_pdf()
            print(response_yes)
        else:
            print(response_no)

    def send_mails(self):
        response_yes = ''
        response_no = ''
        msg = ''
        # Wait for the user to validate the sending of mails
        if self.personal['language'] == 'FR':
            msg = "Voulez-vous envoyer les mails? (O/n)"
            response_yes = "Envoi des mails."
            response_no = "Vous avez la possibilité de les envoyer plus tard. (TODO)"
        if self.personal['language'] == "EN":
            msg = "Do you want to send the mails? (Y/n)"
            response_yes = "The emails are being sent."
            response_no = "You can always send tje emails later. (TODO)"
        ret = input(msg)
        ret = ret.lower()
        if ret == 'o' or ret == 'y' or ret == '':
            self.m.connect()
            for application_parameters in self.tab_lines:
                ret = self.m.send_mail(personal=self.personal, files=self.files,
                                       company=application_parameters['company'],
                                       cover_letter_filename=application_parameters['filename'],
                                       recipient=application_parameters['email'])
                if ret:
                    logger.error("Cannot send mail {}".format(application_parameters['filename']))
            # self.m.close_connection()
            print(response_yes)
        else:
            print(response_no)

    def start(self):
        """
        Start the application in console mode
        :return: 0
        """
        ret = self.get_parameters()
        if ret:
            logger.critical("verify your config file")
            return 1

        self.letter_generation()
        if not self.tab_lines:
            msg = "Error during table handling"
            logger.error(msg)
            return 1
        self.pdf_generation()
        self.send_mails()
        return 0

    #############################################################
    # GUI versions of the modules
    #############################################################

    def letter_generation_gui(self, gen):
        """
        Mail and letter generation: GUI version
        :param gen: generation of the letters: bool
        :return: None
        """
        self.tabular(start_item=0, stop_item=1000, gen=gen)

    def pdf_generation_gui(self):
        """
        Generate the pdf from the .docx files: GUI version
        :return: None
        """
        #these line should be useless since convert_to_pdf.sh was moved to opus/bin
        # FIXME " potential bug here if sendfiles not in "
        out_dir = self.files['out_dir']
        #data_dir = out_dir + '../'
        self.word.set_out_dir(out_dir)
        self.word.set_data_dir(self.data_dir)
        msg = self.word.conversion_to_pdf()
        return msg

    def send_mails_gui(self):
        """
        Send the emails: Gui version
        :return: Status message
        """
        msg = ""
        self.m.connect()
        for application_parameters in self.tab_lines:
                ret = self.m.send_mail(personal=self.personal, files=self.files,
                                       company=application_parameters['company'],
                                       cover_letter_filename=application_parameters['filename'],
                                       recipient=application_parameters['email'])
                msg += "Mail sent to {} \n".format(application_parameters['company'])
                if ret:
                    msg += "Cannot send mail {} \n".format(application_parameters['filename'])
                    logger.error("Cannot send mail {}".format(application_parameters['filename']))
        return msg

    def set_parameters_gui(self, files={}, personal={}, smtp={}, path='./', data_dir='', config_file=''):
        self.files = files
        self.personal = personal
        self.smtp = smtp
        self.path = path
        self.config_file = config_file
        self.data_dir = data_dir
