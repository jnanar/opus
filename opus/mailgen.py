#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
# # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#

import smtplib
# Import the email modules we'll need
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import os
import logging

logger = logging.getLogger('opus.mailgen')


class Mail:
    def __init__(self, data_dir, my_name, my_email, smtp_data):
        self.smtp = smtp_data
        self.s = None
        if self.smtp['user'] is not None:
            self.user = self.smtp['user']
        if self.smtp['password'] is not None:
            self.user = self.smtp['password']
        self.data_dir = data_dir
        self.my_name = my_name
        self.my_email = my_email

    def connect(self):
        self.s = smtplib.SMTP(self.smtp['server'], self.smtp['port'])
        if self.smtp['user'] != 'None' and self.smtp['password'] != 'None':
            # starttls
            self.s.ehlo()
            self.s.starttls()
            self.s.ehlo
            self.s.login(self.smtp['user'], self.smtp['password'])

    def file(self, mail_txt, lang, gender, name, sector, company):
        """

        :param mail_txt: mail text
        :param lang: language
        :param gender: To who the mail is adressed (Sir, Madam or both)
        :param name: name of the recipient
        :param sector: sector of the company
        :param company: name of the company
        :return: the content of the mail
        """
        try:
            fp = open(mail_txt, encoding="utf-8")
        except FileNotFoundError:
            logger.error("File {} not found".format(mail_txt))
            return None
        # Create a text/plain message
        msg = fp.read()

        logger.info("Generation of the email")
        if name is not '':
            msg = msg.replace("%%GENDER%%", gender + ' ' + name)
        else:
            msg = msg.replace("%%GENDER%%", gender)
        msg = msg.replace("%%SECTOR%%", sector)
        msg = msg.replace("%%COMPANY%%", company)


        fp.close()
        return msg

    def close_connection(self):
        self.s.close()
        self.s.quit()

    def generate_mail(self, application_parameters={},
                      mail_txt_fr=None, mail_txt_en=None,
                      output_dir="", my_name=""):
        """

        :param my_name:
        :param application_parameters: parameters of the company
        :param mail_txt_fr: mail content in french
        :param mail_txt_en: mail content in english
        :param output_dir: output directory
        :return: the name of the mail
        """

        # Open a plain text file for reading.  For this example, assume that
        # the text file contains only ASCII characters.
        # mail = [nom, gender, company, sector, email, subject, user_lang, interest]
        # Default encoding mode set to Quoted Printable. Acts globally!
        # email.Charset.add_charset('utf-8', email.Charset.QP, email.Charset.QP, 'utf-8')

        # Open a plain text file for reading.  For this example, assume that
        # the text file contains only ASCII characters.

        mail_txt = " "
        if application_parameters['lang'] == 'FR':
            mail_txt = mail_txt_fr
        elif application_parameters['lang'] == 'EN':
            mail_txt = mail_txt_en

        you = application_parameters['email']
        company = application_parameters['company']
        msg_content = self.file(mail_txt, application_parameters['lang'],
                                application_parameters['gender'],
                                application_parameters['name'],
                                application_parameters['sector'],
                                application_parameters['company'])

        # ret = self.send_mail(msg_content=msg_content, my_subject=my_subject, me=me, you=you, company=company)
        text = u"" + '\n'
        text += you + '\n'
        text += application_parameters['my_subject'] + '\n'
        text += " " + '\n'
        try:
            text += msg_content
        except TypeError:
            logger.error("Mail error for company {}".format(application_parameters['company']))

        filename = output_dir + "mails.txt"

        mail_filename = output_dir + my_name + '-' + company + ".txt"
        mail_filename = mail_filename.replace(" ", "_")
        with open(mail_filename, 'w', encoding='utf8') as f:
            f.write(text)
        with open(filename, 'a', encoding='utf8') as f:
            f.write("----------------------------------------------------------\n")
            f.write(text)

        return mail_filename

    def send_mail(self, personal={}, files={}, company="", recipient="test@example.com", cover_letter_filename=""):
        """

        :param recipient: email of the recipient
        :param cover_letter_filename:
        :param personal:my personal info
        :param files: paths
        :param company: company name
        :return: 0 if OK, 1 or 2 else.
        """
        cover_letter_filename = cover_letter_filename.replace(" ", "_")
        filename = files['out_dir'] + self.my_name + '-' + company + '.txt'
        filename = filename.replace(" ", "_")
        mail_content = ""
        try:
            with open(filename, 'r', encoding='utf8') as f:
                mail_full = f.readlines()
                you = mail_full[1]
                my_subject = mail_full[2]
                for line in mail_full[3:]:
                    mail_content += line
            if you != recipient:
                # The recipient has been changed one of those location: repository or mail.txt
                # As a result, opus do not know which one is correct.
                # TODO: add a button to reload the xlsx file if it has been changed.
                logger.error("Recipient is not identical in generated mail and XLSX file. Abort")
                return 1
        except FileNotFoundError:
            logger.error("Mail txt for {} not found".format(company))
            return 2

        msg = MIMEMultipart()
        try:
            content = MIMEText(mail_content)
            msg.attach(content)
        except AttributeError:
            return 1

        msg['Subject'] = my_subject
        msg['From'] = personal['email']
        msg['To'] = recipient
        bcc = [personal['email']]
        dest_addr = [recipient] + bcc
        cv = files['cv_filename']
        if not os.path:
            msg = "CV file {} does not exist.".format(cv)
            logger.critical(msg)
            return 1
        cv_new_filename = "CV_" + self.my_name + ".pdf"
        cover_letter_new_filename = self.my_name + ".pdf"
        cover_letter = files['out_dir'] + cover_letter_filename
        try:
            part1 = MIMEApplication(open(cv, "rb").read())
        except FileNotFoundError:
            logger.error("File {} not found".format(cv))
            return 1
        try:
            part2 = MIMEApplication(open(cover_letter, "rb").read())
        except FileNotFoundError:
            logger.error("PDF file {} not found".format(cover_letter))
            return 1
        part1.add_header('Content-Disposition', 'attachment', filename=cv_new_filename)
        msg.attach(part1)
        part2.add_header('Content-Disposition', 'attachment', filename=cover_letter_new_filename)
        msg.attach(part2)
#        self.s.set_debuglevel(1)None
        self.s.sendmail(personal['email'], dest_addr, msg.as_string())
        msg = "Email sent to " + company
        logger.info(msg)
        return 0


# ----------------------------------------------------------------------
if __name__ == "__main__":
    my_name = "John Doe"
    data_dir = "../data/"
    smtpdata = {'user': '', 'password': '', 'server': 'relay.proximus.be'}
    m = Mail(data_dir, my_name, "test@example.com", smtpdata)
    line = {}
    cv_path = data_dir
    cv_filename = "test.pdf"
    cover_letter_filename = "cover_letter_company.pdf"
    cover_letter_path = data_dir
