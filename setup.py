#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
# # @author: Arnaud Joset
#
#  This file is part of Opus.
#
# Opus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opus.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import shutil
import sys
from setuptools import setup, find_packages
from cx_Freeze import setup, Executable


# Dependencies are automatically detected, but it might need fine tuning.

#buildOptions = dict(include_files = ['your_folder/'])
includefiles = ['Readme.md', 'Readme_fr.md', 'LICENSE', \
    'data', 'opus.ini', 'data/', 'icon/', 'windows_setup/platforms/']
include_modules = ['PyQt5']
build_exe_options = {"packages": ["os"], "excludes": [], 'include_files': includefiles, 'includes': include_modules}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"
    
    


VERSION = '1.0'

long_description = '''Opus is a python program. It aims to help to find a job.
It can helps to send multiple motivation letters and CVs from templates previously wrote by the applicant.'''

# Is wheel ?
is_wheel = 'bdist_wheel' in sys.argv

excluded = []
if is_wheel:
    excluded.append('extlibs.future')


def exclude_package(pkg):
    for exclude in excluded:
        if pkg.startswith(exclude):
            return True
    return False


def create_package_list(base_package):
    return ([base_package] +
            [base_package + '.' + pkg
             for pkg
             in find_packages(base_package)
             if not exclude_package(pkg)])


setup_info = dict(
    # Metadata
    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    name='opus',
    version=VERSION,
    author='Arnaud Joset',
    author_email='info@agayon.be',
    url='https://gitlab.com/jnanar/opus',
    download_url='https://gitlab.com/jnanar/opus/tree/master',
    description='Opus help you to send cover letters and your CV',
    long_description=long_description,
    license='GPL-3.0',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Win32 (MS Windows)',
        'Environment :: X11 Applications',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: Microsoft :: Windows',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.6',
        'Topic :: Desktop Environment',
        'Topic :: Utilities',
    ],
    scripts=['opus/bin/convert_to_pdf.sh'],
    install_requires=['xlrd', 'pyqt5'],

    # Package info
    packages=create_package_list('opus'),
    zip_safe=True,
    options = {"build_exe": build_exe_options},
    executables = [Executable("opus_win.py", base=base)],
)

setup(**setup_info)
